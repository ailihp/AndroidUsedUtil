package com.ailihp.developer.androidusedutil

import android.app.Application
import com.ailihp.developer.androidusedutil.utils.preference.AppPreference

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        AppPreference.init(this)
    }
}