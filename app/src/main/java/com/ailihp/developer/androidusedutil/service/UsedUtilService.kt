package com.ailihp.developer.androidusedutil.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import com.ailihp.developer.androidusedutil.R
import com.ailihp.developer.androidusedutil.config.NOTIFICATION_CHANNEL_ID
import com.ailihp.developer.androidusedutil.config.NOTIFICATION_ID
import com.ailihp.developer.androidusedutil.service.controller.UsedUtilServiceController
import com.ailihp.developer.androidusedutil.utils.DLog

class UsedUtilService : Service() {

    private val mController: UsedUtilServiceController by lazy {
        UsedUtilServiceController(this)
    }

    override fun onBind(intent: Intent): IBinder? {
        DLog.v("onBind")
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val notification =
            NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setWhen(0)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setAutoCancel(false)
                .build()
        startForeground(NOTIFICATION_ID, notification)
        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
        DLog.v("onCreate")
        mController.onCreate()
    }

    override fun onDestroy() {
        super.onDestroy()
        DLog.v("onDestroy")
        mController.onDestroy()
    }

}