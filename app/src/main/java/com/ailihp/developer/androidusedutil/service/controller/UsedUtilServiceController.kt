package com.ailihp.developer.androidusedutil.service.controller

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import com.ailihp.developer.androidusedutil.config.EXTRA_KEY_GIF_URL
import com.ailihp.developer.androidusedutil.utils.DLog
import com.ailihp.developer.androidusedutil.utils.ascii.ASCIIEndcoder
import com.ailihp.developer.androidusedutil.utils.preference.AppPreference
import com.ailihp.developer.androidusedutil.view.GifActivity

class UsedUtilServiceController(private val mContext: Context) :
    ClipboardManager.OnPrimaryClipChangedListener {
    private val TAG = UsedUtilServiceController::class.java.simpleName

    private fun getClipboardManager(): ClipboardManager {
        return mContext.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }

    fun onCreate() {
        // Add ClipBoard Changed Listener
        getClipboardManager().addPrimaryClipChangedListener(this)
    }

    fun onDestroy() {
        // Remove ClipBoard Changed Listener
        getClipboardManager().removePrimaryClipChangedListener(this)
    }

    override fun onPrimaryClipChanged() {
        if (!AppPreference.getClipboardObserver()) {
            DLog.v("Clipboard observing is false")
            return
        }

        val clipData = getClipboardManager().primaryClip

        if (null == clipData) {
            DLog.e("Clip Data is null")
            return
        }
        replaceData(clipData)
    }

    private fun replaceData(clipData: ClipData) {
        val dataCount = clipData.itemCount

        for (i in 0 until dataCount) {
            DLog.v("Clip : ${clipData.getItemAt(i)}")

            var clipText = clipData.getItemAt(i).text.toString()
            DLog.v("Clip Data : $clipText")

            if (clipText.isNotEmpty()) {
                clipText = clipText.trim()
                clipText = ASCIIEndcoder.encode(clipText)
                DLog.v("Replace Clip Data : $clipText")
                val realUrl = getRealGifUrl(clipText)
                DLog.v("Gif URL : $realUrl")
                if(realUrl.isNotEmpty() && realUrl.contains("gif")) {
                    DLog.v("View Gif")
                    mContext.startActivity(Intent(mContext, GifActivity::class.java).apply {
                        putExtra(EXTRA_KEY_GIF_URL, realUrl)
                    })
                }
            }
        }
    }

    private fun getRealGifUrl(url: String): String {
        try {
            val srcKey = "src=\""
            val quiteIndex = url.indexOf(srcKey)
            DLog.v("scr index : $quiteIndex")
            val subString1 = url.substring(quiteIndex + srcKey.length, url.length)
            DLog.v("subString1 : $subString1")
            val quiteIndex2 = subString1.indexOf("\"")
            return subString1.substring(0, quiteIndex2)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return url
    }
}