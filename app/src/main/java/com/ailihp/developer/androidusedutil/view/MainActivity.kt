package com.ailihp.developer.androidusedutil.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.ailihp.developer.androidusedutil.R
import com.ailihp.developer.androidusedutil.service.UsedUtilService
import com.ailihp.developer.androidusedutil.view.base.BaseActivity

class MainActivity : BaseActivity(), View.OnClickListener {
    override fun getLayoutRes() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        findViewById<Button>(R.id.button_start).setOnClickListener(this)
        findViewById<Button>(R.id.button_finish).setOnClickListener(this)

    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.button_start -> {
                startService(Intent(this, UsedUtilService::class.java))
            }

            R.id.button_finish -> {
                stopService(Intent(this, UsedUtilService::class.java))
            }
        }
    }
}
