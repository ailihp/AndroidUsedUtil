package com.ailihp.developer.androidusedutil.view

import android.os.Bundle
import android.widget.ImageButton
import android.widget.ImageView
import com.ailihp.developer.androidusedutil.R
import com.ailihp.developer.androidusedutil.config.EXTRA_KEY_GIF_URL
import com.ailihp.developer.androidusedutil.view.base.BaseActivity
import com.bumptech.glide.Glide

class GifActivity : BaseActivity() {
    override fun getLayoutRes() = R.layout.activity_gif

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val imageView = findViewById<ImageView>(R.id.imageView)

        val url = intent?.extras?.getString(EXTRA_KEY_GIF_URL) ?: ""
        Glide.with(this).load(url).into(imageView)

        val buttonClose = findViewById<ImageButton>(R.id.imageButton_close)
        buttonClose.setOnClickListener {
            finish()
        }
    }

}
