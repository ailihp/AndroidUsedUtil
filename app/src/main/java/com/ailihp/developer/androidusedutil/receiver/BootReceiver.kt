package com.ailihp.developer.androidusedutil.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.ailihp.developer.androidusedutil.service.UsedUtilService

class BootReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent) {
        when (intent.action) {
            Intent.ACTION_BOOT_COMPLETED -> {
                context?.startService(Intent(context, UsedUtilService::class.java))
            }
            else -> {
                // Nothing
            }
        }
    }
}