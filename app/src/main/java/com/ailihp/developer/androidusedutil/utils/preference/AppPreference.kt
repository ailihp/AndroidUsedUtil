package com.ailihp.developer.androidusedutil.utils.preference

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

/**
 * Created by hmlee on 2018-08-14.
 */
object AppPreference {
    private lateinit var preferences: SharedPreferences

    private val KEY_CLIP_BOARD_OBSERVER = "key_clip_board_observer"

    /**
     * Please Call init() in Application Class
     *
     * @param ctx Context
     */
    fun init(ctx: Context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(ctx)
    }

    /**
     * Put Preferences
     *
     * @param key Preferences Key
     * @param value AnyType Value
     */
    fun put(key: String, value: Any) {
        preferences.edit().apply {
            when (value) {
                is String -> this.putString(key, value)
                is Int -> this.putInt(key, value)
                is Boolean -> this.putBoolean(key, value)
                is Float -> this.putFloat(key, value)
                is Long -> this.putLong(key, value)
                else -> throw Exception("is not define type cast")
            }
        }.apply()
    }

    /**
     * Get Preferences Value
     *
     * @param key Preferences key
     * @param defValue Preferences Default Value
     * @return Saved Preferences Value
     */
    fun <T> get(key: String, defValue: T): T {
        @Suppress("UNCHECKED_CAST")
        return when (defValue) {
            is String -> preferences.getString(key, defValue) as T
            is Int -> preferences.getInt(key, defValue) as T
            is Boolean -> preferences.getBoolean(key, defValue) as T
            is Float -> preferences.getFloat(key, defValue) as T
            is Long -> preferences.getLong(key, defValue) as T
            else -> throw Exception("is not define type cast")
        }
    }

    fun setClipboardObserver(isObserving: Boolean) {
        put(KEY_CLIP_BOARD_OBSERVER, isObserving)
    }

    fun getClipboardObserver() = get(KEY_CLIP_BOARD_OBSERVER, true)
}