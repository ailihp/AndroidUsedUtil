package com.ailihp.developer.androidusedutil.config

@JvmField val NOTIFICATION_ID = 100
@JvmField val NOTIFICATION_CHANNEL_ID = "ailihp_android_used_util"


@JvmField val EXTRA_KEY_GIF_URL = "EXTRA_KEY_GIF_URL"
