package com.ailihp.developer.androidusedutil.utils.ascii

import com.ailihp.developer.androidusedutil.utils.DLog

object ASCIIEndcoder {
    enum class ASCII(val ascii: String, val word: String) {
        SPACE("%20", " "),
        COLON("%3A", ":"),
        SLASH("%2F", "/"),
        QUESTION("%3F", "?"),
        AND("%26", "&"),
        EQUAL("%3D", "="),
        PERCENT("%25", "%"),
        DOUBLE_QUOTE("%22", "\"")
    }

    fun encode(str: String): String {
        var encodeString: String = str
        for (key in ASCII.values()) {
            if (encodeString.contains(key.ascii)) {
                encodeString = replace(encodeString)
                return encode(encodeString)
            }
        }

        return encodeString
    }

    private fun replace(str: String): String {
        var encodeString: String = str
        for (key in ASCII.values()) {
            encodeString = encodeString.replace(key.ascii, key.word)
        }
        return encodeString
    }
}