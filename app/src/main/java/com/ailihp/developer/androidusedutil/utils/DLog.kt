package com.ailihp.developer.androidusedutil.utils

import android.util.Log
import com.ailihp.developer.androidusedutil.BuildConfig

object DLog {
    private val TAG = "DEBUG"

    fun v(msg: String) {
        if(BuildConfig.DEBUG) {
            Log.v(TAG, msg)
        }
    }

    fun i(msg: String) {
        if(BuildConfig.DEBUG) {
            Log.i(TAG, msg)
        }
    }

    fun w(msg: String) {
        if(BuildConfig.DEBUG) {
            Log.w(TAG, msg)
        }
    }

    fun e(msg: String) {
        if(BuildConfig.DEBUG) {
            Log.e(TAG, msg)
        }
    }

    fun d(msg: String) {
        if(BuildConfig.DEBUG) {
            Log.d(TAG, msg)
        }
    }
}